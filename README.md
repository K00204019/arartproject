# ARArtProject

Rational
For my Augmented Reality Art project I decided to create a simple but effective  project to show butterflys landing on flowers and flapping their wings. I came up with this idea because I thought people would find it interesting if they seen a poster of flowers and held their phone up to it and butterflys were there. I first got an image of flowers to use as my poster base. It was difficult to get an image that had enough target points on it to be able to work. I eventually found a nice one of pink flowers that was good quality and had 4 stars on vuforia. I imported the vuforia files into my unity project. I then created some simple butterflys and placed then on top of the flowers in unity. I added an animation to the butterflys so that they would flap their wings when the project was played. To test the project I played it in unity and held a print out of the image up to the the camera and the butterflys were there flapping their wings on the flowers. Even if I moved the image around the butterflys still stick to the same place.


